/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.monk.oxprogramoop;

import java.util.Scanner;

/**
 *
 * @author acer
 */
public class Game {

    Player playerX;
    Player playerO;
    Player turn;
    Table table;
    int row, col;
    String k;
    Scanner kb = new Scanner(System.in);

    public Game() {
        playerX = new Player('X');
        playerO = new Player('O');
        table = new Table(playerX, playerO);
    }

    public void ShowWelcome() {
        System.out.println("Welcome to OX game");
    }

    public void showTable() {
        table.showTable();
    }

    public void input() {
        while (true) {
            System.out.println("Please input Row Col:");
            row = kb.nextInt() - 1;
            col = kb.nextInt() - 1;

            if (table.setRowCol(row, col)) {
                break;
            }
            System.out.println("Error: This table is not available! ");
        }
    }

    public void ShowTurn() {
        System.out.println(table.getCurrnentPlayer().getName() + " turn");
    }

    public void newGame() {
        table = new Table(playerX, playerO);
    }

    public void run() {
        this.ShowWelcome();
        while (true) {
            this.showTable();
            this.ShowTurn();
            this.input();
            table.checkWin();

            if (table.isFinish()) {
                if (table.getWinner() == null) {
                    System.out.println("Draw!!");
                } else {
                    System.out.println(table.getWinner().getName() + " win!!");
                }
                System.out.println("Do you want to continue playing?");
                System.out.println("Please type yes or no:");
                k = kb.next();
                if (k.equals("yes")) {
                    newGame();
                } else {
                    System.out.println("Bye Bye");
                    break;
                }

            }
            table.switcPlayer();
        }
    }
}
