/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.monk.oxprogramoop;

/**
 *
 * @author acer
 */
public class Table {

    private char[][] table
            = {{'-', '-', '-'}, {'-', '-', '-'}, {'-', '-', '-'}};
    private Player playerX;
    private Player playerO;
    private Player currentPlayer;
    private Player winner = null;
    private boolean finish = false;
    private int lastcol;
    private int lastrow;

    public Table(Player X, Player O) {
        playerX = X;
        playerO = O;
        currentPlayer = X;
    }

    public void showTable() {
        System.out.println("  1 2 3");
        for (int row = 0; row < table.length; row++) {
            System.out.print(row + 1 + " ");
            for (int col = 0; col < table[row].length; col++) {
                System.out.print(table[row][col] + " ");
            }
            System.out.println("");

        }
        System.out.println("");
    }

    public boolean setRowCol(int row, int col) {
        if (table[row][col] == '-') {
            table[row][col] = currentPlayer.getName();
            this.lastrow = row;
            this.lastcol = col;
            return true;
        }
        return false;
    }

    public Player getCurrnentPlayer() {
        return currentPlayer;
    }

    public void switcPlayer() {
        if (currentPlayer == playerX) {
            currentPlayer = playerO;
        } else {
            currentPlayer = playerX;
        }
    }

    void checkCol() {
        for (int row = 0; row < 3; row++) {
            if (table[row][lastcol] != currentPlayer.getName()) {
                return;
            }
        }
        finish = true;
        winner = currentPlayer;
        setStatWinLose();
    }

    private void setStatWinLose() {
        if (currentPlayer == playerO) {
            playerO.win();
            playerX.lose();
        } else {
            playerO.lose();
            playerX.win();
        }
    }

    void checkRow() {
        for (int col = 0; col < 3; col++) {
            if (table[lastrow][col] != currentPlayer.getName()) {
                return;
            }
        }
        finish = true;
        winner = currentPlayer;
        setStatWinLose();
    }

    void checkDiagonal() {

        if (table[2][2] != '-') {
            int n = 0;
            for (lastrow = 0, lastcol = 0; lastcol < 2; lastcol++, lastrow++) {
                if (table[lastrow][lastcol] == currentPlayer.getName()) {
                    n++;
                    if (n == 2) {
                        winner = currentPlayer;
                        setStatWinLose();
                        finish = true;
                        return;
                    }
                }
            }
        }
        if (table[1][1] != '-') {
            int b = 0;
            for (lastrow = 2, lastcol = 0; lastcol <= 2; lastcol++, lastrow--) {
                if (table[lastrow][lastcol] == currentPlayer.getName()) {
                    b++;
                    if (b == 3) {
                        winner = currentPlayer;
                        setStatWinLose();
                        finish = true;
                        return;
                    }
                }
            }
        }
    }

    void checkDraw() {
        for (int row = 0; row < 3; row++) {
            for (int col = 0; col < 3; col++) {
                if (table[row][col] == '-') {
                    return;
                }
            }
        }
        
        finish = true;
        playerO.draw();
        playerX.draw();
    }

    public void checkWin() {
        checkRow();
        checkCol();
        checkDiagonal();
        checkDraw();
    }

    public boolean isFinish() {
        return finish;
    }

    public Player getWinner() {
        return winner;
    }

    public void chekWin() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
