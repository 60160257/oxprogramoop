/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import com.monk.oxprogramoop.Player;
import com.monk.oxprogramoop.Table;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

/**
 *
 * @author acer
 */
public class TestTable {
    
    public TestTable() {
    }
    
    @BeforeAll
    public static void setUpClass() {
    }
    
    @AfterAll
    public static void tearDownClass() {
    }
    
    @BeforeEach
    public void setUp() {
    }
    
    @AfterEach
    public void tearDown() {
    }

    // TODO add test methods here.
    // The methods must be annotated with annotation @Test. For example:
    //
    // @Test
    // public void hello() {}
    @Test
    public void testRow1byX(){
        Player X = new Player('X');
        Player O = new Player('O');
        Table table = new Table(X, O);
        table.setRowCol(0, 0);
        table.setRowCol(0, 1);
        table.setRowCol(0, 2);
        table.checkWin();
        assertEquals(true, table.isFinish());
        assertEquals(X, table.getWinner());
    }
    
     @Test
    public void testRow2byX(){
        Player X = new Player('X');
        Player O = new Player('O');
        Table table = new Table(X, O);
        table.setRowCol(1, 0);
        table.setRowCol(1, 1);
        table.setRowCol(1, 2);
        table.checkWin();
        assertEquals(true, table.isFinish());
        assertEquals(X, table.getWinner());
    }
    
     @Test
    public void testRow3byX(){
        Player X = new Player('X');
        Player O = new Player('O');
        Table table = new Table(X, O);
        table.setRowCol(2, 0);
        table.setRowCol(2, 1);
        table.setRowCol(2, 2);
        table.checkWin();
        assertEquals(true, table.isFinish());
        assertEquals(X, table.getWinner());
    }
    
     @Test
    public void testcol1byX(){
        Player X = new Player('X');
        Player O = new Player('O');
        Table table = new Table(X, O);
        table.setRowCol(0, 0);
        table.setRowCol(1, 0);
        table.setRowCol(2, 0);
        table.checkWin();
        assertEquals(true, table.isFinish());
        assertEquals(X, table.getWinner());
    }
    
     @Test
    public void testcol2byX(){
        Player X = new Player('X');
        Player O = new Player('O');
        Table table = new Table(X, O);
        table.setRowCol(0, 1);
        table.setRowCol(1, 1);
        table.setRowCol(2, 1);
        table.checkWin();
        assertEquals(true, table.isFinish());
        assertEquals(X, table.getWinner());
    }
    
     @Test
    public void testcol3byX(){
        Player X = new Player('X');
        Player O = new Player('O');
        Table table = new Table(X, O);
        table.setRowCol(0, 2);
        table.setRowCol(1, 2);
        table.setRowCol(2, 2);
        table.checkWin();
        assertEquals(true, table.isFinish());
        assertEquals(X, table.getWinner());
    }
    
     @Test
    public void testRowcolX1byX(){
        Player X = new Player('X');
        Player O = new Player('O');
        Table table = new Table(X, O);
        table.setRowCol(0, 0);
        table.setRowCol(1, 1);
        table.setRowCol(2, 2);
        table.checkWin();
        assertEquals(true, table.isFinish());
        assertEquals(X, table.getWinner());
    }
    
      @Test
    public void testRowcolX2byX(){
        Player X = new Player('X');
        Player O = new Player('O');
        Table table = new Table(X, O);
        table.setRowCol(0, 2);
        table.setRowCol(1, 1);
        table.setRowCol(2, 0);
        table.checkWin();
        assertEquals(true, table.isFinish());
        assertEquals(X, table.getWinner());
    }
    
     @Test
    public void testRow1byO(){
        Player X = new Player('X');
        Player O = new Player('O');
        Table table = new Table(O, X);
        table.setRowCol(0, 0);
        table.setRowCol(0, 1);
        table.setRowCol(0, 2);
        table.checkWin();
        assertEquals(true, table.isFinish());
        assertEquals(O, table.getWinner());
    }
    
     @Test
    public void testRow2byO(){
        Player X = new Player('X');
        Player O = new Player('O');
        Table table = new Table(O, X);
        table.setRowCol(1, 0);
        table.setRowCol(1, 1);
        table.setRowCol(1, 2);
        table.checkWin();
        assertEquals(true, table.isFinish());
        assertEquals(O, table.getWinner());
    }
    
     @Test
    public void testRow3byO(){
        Player X = new Player('X');
        Player O = new Player('O');
        Table table = new Table(O, X);
        table.setRowCol(2, 0);
        table.setRowCol(2, 1);
        table.setRowCol(2, 2);
        table.checkWin();
        assertEquals(true, table.isFinish());
        assertEquals(O, table.getWinner());
    }
    
     @Test
    public void testcol1byO(){
        Player X = new Player('X');
        Player O = new Player('O');
        Table table = new Table(O, X);
        table.setRowCol(0, 0);
        table.setRowCol(1, 0);
        table.setRowCol(2, 0);
        table.checkWin();
        assertEquals(true, table.isFinish());
        assertEquals(O, table.getWinner());
    }
    
     @Test
    public void testcol2byO(){
        Player X = new Player('X');
        Player O = new Player('O');
        Table table = new Table(O, X);
        table.setRowCol(0, 1);
        table.setRowCol(1, 1);
        table.setRowCol(2, 1);
        table.checkWin();
        assertEquals(true, table.isFinish());
        assertEquals(O, table.getWinner());
    }
    
     @Test
    public void testcol3byO(){
        Player X = new Player('X');
        Player O = new Player('O');
        Table table = new Table(O, X);
        table.setRowCol(0, 2);
        table.setRowCol(1, 2);
        table.setRowCol(2, 2);
        table.checkWin();
        assertEquals(true, table.isFinish());
        assertEquals(O, table.getWinner());
    }
    
     @Test
    public void testRowcolX1byO(){
        Player X = new Player('X');
        Player O = new Player('O');
        Table table = new Table(O, X);
        table.setRowCol(0, 0);
        table.setRowCol(1, 1);
        table.setRowCol(2, 2);
        table.checkWin();
        assertEquals(true, table.isFinish());
        assertEquals(O, table.getWinner());
    }
    
      @Test
    public void testRowcolX2byO(){
        Player X = new Player('X');
        Player O = new Player('O');
        Table table = new Table(O, X);
        table.setRowCol(0, 2);
        table.setRowCol(1, 1);
        table.setRowCol(2, 0);
        table.checkWin();
        assertEquals(true, table.isFinish());
        assertEquals(O, table.getWinner());
    }
    @Test
    public void testRowcolIsNotEmty1(){
        Player X = new Player('X');
        Player O = new Player('O');
        Table table = new Table(O, X);
        table.setRowCol(0, 0);
        table.setRowCol(0, 2);
        table.setRowCol(2, 2);
        table.setRowCol(2, 0);
        table.checkWin();
        assertEquals(false, table.isFinish());
    }
    
    @Test
    public void testRowcolIsNotEmty2(){
        Player X = new Player('X');
        Player O = new Player('O');
        Table table = new Table(O, X);
        table.setRowCol(0, 0);
        table.checkWin();
        assertEquals(false, table.setRowCol(0, 0));
    }
    @Test
    public void testRowcolDraw(){
        Player X = new Player('X');
        Player O = new Player('O');
        Table table = new Table(X, O);
        table.setRowCol(0, 1);
        table.setRowCol(0, 0);
        table.setRowCol(1, 1);
        table.setRowCol(0, 2);
        table.setRowCol(1, 2);
        table.setRowCol(1, 0);
        table.setRowCol(2, 0);
        table.setRowCol(2, 1);
        table.setRowCol(2, 2);
        table.checkWin();
        assertEquals(true, table.isFinish());
        assertEquals(table.getWinner(), table.getCurrnentPlayer());
        
    }
}
